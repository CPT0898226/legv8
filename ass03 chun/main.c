/* main.c simple program to test assembler program */

#include <stdio.h>

extern unsigned long long int multiply(unsigned int a, unsigned int b);

int main(void)
{
    long long int a = multiply(0, 0);
    printf("Result of multiply(0, 0) = %lu\n", a);
    a = multiply(0, 1);
	printf("Result of multiply(0, 1) = %lu\n", a);
	a = multiply(1, 0);
	printf("Result of multiply(1, 0) = %lu\n", a);
	a = multiply(1, 1);
	printf("Result of multiply(1, 1) = %lu\n", a);
	a = multiply(1, 4294967295);
	printf("Result of multiply(1, 4294967295) = %lu\n", a);
	a = multiply(4294967295, 1);
	printf("Result of multiply(4294967295, 1) = %lu\n", a);
	a = multiply(4294967295, 4294967295);
	printf("Result of multiply(4294967295, 4294967295) = %lu\n", a);
    return 0;
}
